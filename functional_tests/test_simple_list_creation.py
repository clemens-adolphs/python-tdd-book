from .base import FunctionalTest

from selenium import webdriver
from selenium.webdriver.common.keys import Keys


class NewVisitorTest(FunctionalTest):

    def test_can_start_a_list_and_retrieve_it_later(self):
        # Our cool end user opens up their web browser:
        self.browser.get(self.live_server_url)

        # They notice that our page title / header mention to-do lists:
        self.assertIn('To-Do', self.browser.title)

        header_text = self.browser.find_element_by_tag_name('h1').text
        self.assertIn('To-Do', header_text)

        # Now our user is invited to enter a to-do item right away
        inputbox = self.get_item_input_box()
        self.assertEqual(inputbox.get_attribute('placeholder'),
                'Enter a to-do item')

        # User types "Buy more beer" into a text box
        inputbox.send_keys('Buy more beer')

        # When he hits enter, the page updates, and now the page lists
        # "1: Buy more beer" as an item in a to-do list
        inputbox.send_keys(Keys.ENTER)

        self.wait_for_row_in_list_table('1: Buy more beer')

        # There is still a text box inviting him to enter another item.

        # He enters "Drink beer"
        inputbox = self.get_item_input_box()
        inputbox.send_keys('Drink beer')
        inputbox.send_keys(Keys.ENTER)

        self.wait_for_row_in_list_table('1: Buy more beer')
        self.wait_for_row_in_list_table('2: Drink beer')


    def test_multiple_users_can_start_lists_at_different_urls(self):
        # Our user starts a new to-do list
        self.browser.get(self.live_server_url)
        inputbox = self.get_item_input_box()
        inputbox.send_keys('Buy more beer')
        inputbox.send_keys(Keys.ENTER)
        self.wait_for_row_in_list_table('1: Buy more beer')

        bruno_list_url = self.browser.current_url
        self.assertRegex(bruno_list_url, 'lists/.+')

        # Now a new user, Gerd, comes along to the site.

        ## We use a new browser session to make sure that no information of
        ## Bruno's is coming through from cookies etc
        self.browser.quit()
        self.browser = webdriver.Firefox()

        # Gerd visits the homepage. There is no sign of Bruno's list.
        self.browser.get(self.live_server_url)
        page_text = self.browser.find_element_by_tag_name('body').text
        self.assertNotIn('Buy more beer', page_text)
        self.assertNotIn("Drink beer", page_text)

        # Gerd starts a new list by entering a new item.
        inputbox = self.get_item_input_box()
        inputbox.send_keys('Buy milk')
        inputbox.send_keys(Keys.ENTER)
        self.wait_for_row_in_list_table('1: Buy milk')

        # Gerd gets his own unique url
        gerd_list_url = self.browser.current_url
        self.assertRegex(gerd_list_url, '/lists/.+')
        self.assertNotEqual(bruno_list_url, gerd_list_url)

        # Again, there is no trace of Bruno's list
        page_text = self.browser.find_element_by_tag_name('body').text
        self.assertNotIn('Buy more beer', page_text)
        self.assertIn("Buy milk", page_text)

        # Satisfied, they go back to sleep
