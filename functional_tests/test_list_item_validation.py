from .base import FunctionalTest

from selenium.webdriver.common.keys import Keys


class ItemValidationTest(FunctionalTest):

    def test_cannot_add_an_empty_list(self):
        # Bruno goes to the homepage and accidentally tries to submit an
        # empty list item. He hits enter on the input box.
        self.browser.get(self.live_server_url)

        self.get_item_input_box().send_keys(Keys.ENTER)

        # The browser intercepts the request, and does not load the
        # list page
        self.wait_for(lambda: self.browser.find_elements_by_css_selector(
            '#id_text:invalid'
            ))

        # He starts typing some text for the new item and the error disappears
        self.get_item_input_box().send_keys('Buy beer')
        self.wait_for(lambda: self.browser.find_elements_by_css_selector('#id_text:valid'))

        # And he can submit successfully:
        self.get_item_input_box().send_keys(Keys.ENTER)
        self.wait_for_row_in_list_table('1: Buy beer')

        # Perversely, he now decides to submit a second blank list item
        self.get_item_input_box().send_keys(Keys.ENTER)

        # Again, the browser will not comply
        self.wait_for_row_in_list_table('1: Buy beer')
        self.wait_for(lambda: self.browser.find_elements_by_css_selector('#id_text:valid'))

        # He can correct it by filling in some text.
        self.get_item_input_box().send_keys('Drink beer')
        self.wait_for(lambda: self.browser.find_elements_by_css_selector('#id_text:valid'))
        self.get_item_input_box().send_keys(Keys.ENTER)

        self.wait_for_row_in_list_table('1: Buy beer')
        self.wait_for_row_in_list_table('2: Drink beer')
